package net.javaguides.addressmanagement.dao;

import net.javaguides.addressmanagement.model.Address;
import net.javaguides.usermanagement.utl.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.function.Consumer;

/**
 * CRUD database operations
 * @author Ramesh Fadatare
 *
 */
public class AddressDao {

    /**
     * Save Address
     * @param address
     */
    public void saveAddress(Address address) {
        operation(session -> session.save(address));
    }

    /**
     * Update Address
     * @param address
     */
    public void updateAddress(Address address) {
        operation(session -> session.update(address));
    }

    /**
     * Delete Address
     * @param id
     */
    public void deleteAddress(int id) {
        operation(session -> session.delete(session.get(Address.class, id)));
    }

    private void operation(Consumer<Session> operation) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the student object

            operation.accept(session);

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * Get Address By ID
     * @param id
     * @return
     */
    public Address getAddress(int id) {

        Transaction transaction = null;
        Address address = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an address object
            address = session.get(Address.class, id);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return address;
    }

    /**
     * Get all Addresss
     * @return
     */
    @SuppressWarnings("unchecked")
    public List < Address > getAllAddress() {

        Transaction transaction = null;
        List < Address > listOfAddress = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an address object

            listOfAddress = session.createQuery("from Address").getResultList();

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return listOfAddress;
    }
}